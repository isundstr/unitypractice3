﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsScript : MonoBehaviour {

	public Rigidbody rb;
	TestingScript testingScript;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();
		testingScript = GetComponent<TestingScript> ();

		testingScript.aWholeNumber = 2675;

		testingScript.PrintHello ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.anyKeyDown) {

			int gravityValue = TurnOnGravity (6, "Heya");
			print ("Gravity value:" + gravityValue);
		}
	}


	public int TurnOnGravity(int numberA, string gravityMessage){

		print (gravityMessage);

		rb.useGravity = true;


		return numberA * 5;
	}
}
